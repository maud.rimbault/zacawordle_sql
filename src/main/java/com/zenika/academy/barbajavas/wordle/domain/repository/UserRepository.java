package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class UserRepository {
    
    private final Map<String, User> usersByTid = new HashMap<>();
    
    public void save(User u) {
        this.usersByTid.put(u.getTid(), u);
    }
    
    public Optional<User> findByEmail(String email) {
        return usersByTid.values().stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst();
    }

    public Optional<User> findByTid(String userTid) {
        return Optional.ofNullable(usersByTid.get(userTid));
    }

    public void delete(String userTid) {
        usersByTid.remove(userTid);
    }
}
