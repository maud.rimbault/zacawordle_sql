package com.zenika.academy.barbajavas.wordle.domain.model.game;

public class GameNotForThisUserException extends Exception {
    public GameNotForThisUserException() {
        super("The game does not belong to this user");
    }
}
